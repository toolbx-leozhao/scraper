import requests
import json
import csv

from bs4 import BeautifulSoup

import pdb

headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}

with open('output.csv', mode='w') as out_file:
	csv_writer = csv.writer(out_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	with open('input.csv') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		for idx, row in enumerate(csv_reader):
			if idx == 0:
				csv_writer.writerow(row)
				continue

			# Skip non home depot products
			sku = row[5]
			if len(sku) != 10:
				csv_writer.writerow(row)
				continue

			try:
				# Static content
				product_url = 'https://www.homedepot.ca/product/{sku}'.format(sku=sku)
				static_content = BeautifulSoup(requests.get(product_url, timeout=10).text, 'html.parser')

				# Dynamic content
				product_price_url = 'https://www.homedepot.ca/homedepotcacommercewebservices/v2/homedepotca/products/{sku}/localized/7073?fields=BASIC_SPA&lastAddedFulfillment=SHIPTOHOME&lang=en'.format(
					sku=sku)
				dynamic_content = json.loads(requests.get(product_price_url, timeout=10, headers=headers).content.decode('utf-8'))

				# Find content
				product_name = static_content.select_one('span.hdca-product__description-title-product-name').getText()
				product_price = dynamic_content.get('optimizedPrice', {}).get('displayPrice', {}).get('value')
				product_stock = dynamic_content.get('storeStock', {}).get('stockLevel')

			except:
				# For one reason or another if we fail then we didn't find the product.
				csv_writer.writerow(row[:6] + [False])
				continue

			if not product_price:
				csv_writer.writerow(row[:6] + [True, "Not available", "Not available", ""])
				continue

			csv_writer.writerow(row[:6] + [True, product_price, product_stock, round(product_price - float(row[4]),2)])

			print "Name: " + product_name + "| Price:" + str(product_price) + "| SKU: " + sku

# Rona
# response.xpath('//div[contains(@class, \'productDetails\')]//span[contains(@class, \'price-box__price__amount__decimal\')]/text()').get()
# response.xpath('//div[contains(@class, \'productDetails\')]//sup[contains(@class, \'price-box__price__amount__decimal\')]/text()').get()